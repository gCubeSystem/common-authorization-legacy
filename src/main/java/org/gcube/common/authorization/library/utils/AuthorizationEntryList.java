package org.gcube.common.authorization.library.utils;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.gcube.common.authorization.library.AuthorizationEntry;

@XmlRootElement
public class AuthorizationEntryList {
	
	private List<AuthorizationEntry> entries = new ArrayList<AuthorizationEntry>();

	protected AuthorizationEntryList() {}
	
	public AuthorizationEntryList(List<AuthorizationEntry> entries) {
		super();
		this.entries = entries;
	}

	public List<AuthorizationEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<AuthorizationEntry> entries) {
		this.entries = entries;
	}
	
}
