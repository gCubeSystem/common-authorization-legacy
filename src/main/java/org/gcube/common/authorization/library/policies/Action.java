package org.gcube.common.authorization.library.policies;

public enum Action {

	ALL, ACCESS, WRITE, DELETE,  EXECUTE;
}
