package org.gcube.common.authorization.library.provider;

@Deprecated
public class AccessTokenProvider {

    public static AccessTokenProvider instance = new AccessTokenProvider();

    private static final InheritableThreadLocal<String> threadToken = new InheritableThreadLocal<String>() {

        @Override
        protected String initialValue() {
            return null;
        }

    };

    private AccessTokenProvider() {
    }

    public String get() {
        return threadToken.get();
    }

    public void set(String jwt) {
        threadToken.set(jwt);
    }

    public void reset() {
        threadToken.remove();
    }
}
