package org.gcube.common.authorization.library.utils;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.gcube.common.authorization.library.provider.ServiceInfo;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MultiServiceTokenRequest {
	
	private List<String> containerTokens= new ArrayList<String>();
	
	private ServiceInfo info;
	
	protected MultiServiceTokenRequest() {}
	
	public MultiServiceTokenRequest(List<String> containerTokens, ServiceInfo info) {
		this.containerTokens = containerTokens;
		this.info = info;
	}

	public List<String> getContainerTokens() {
		return containerTokens;
	}

	public ServiceInfo getInfo() {
		return info;
	}
	
}
