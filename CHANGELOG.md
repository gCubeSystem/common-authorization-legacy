
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.5.1]

- Added library to support Java 11 JDK

## [v2.5.0] - [2022-04-20]

- Deprecated AccessTokenProvider, AuthorizationProvider and SecurityTokenProvider [#22871]
- Added roles to ExternalService Client info

## [v2.4.0] - [2021-05-21]

JWTUmaTokenProvider changed to AccessTokenProvider

## [v2.3.1] - [2021-03-30]

User info returns also email, first name and Last name according to the new UMA token

## [v2.3.0] - [2010-11-17]

support for the new IAM added

## [v2.2.1] - [2019-01-14]

endpoint updated for https

## [v2.1.6] - [2019-01-14]

added ListMapper

## [v2.1.3] - [2019-01-14]

added the support for authorization control library

## [v2.0.2] - [2017-02-27]

added set and reset of scope in AuthorizedTask

## [v2.0.0] - [2016-09-30]

new model for auth2

## [v1.0.0] - [2015-05-18]

First Release
